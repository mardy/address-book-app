 /*
 * Copyright (C) 2012-2015 Canonical, Ltd.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import QtContacts 5.0

import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.3

import Ubuntu.Contacts 0.1
import Ubuntu.AddressBook.Base 0.1

ContactDetailBase {
    id: root

    property alias active: sourceModel.autoUpdate
    property bool isNewContact: contact && contact.contactId === "qtcontacts:::"
    property real myHeight: label.height + units.gu(6) + (sources.currentlyExpanded ? sources.containerHeight :
                                                                                      sources.itemHeight)

    signal changed()

    function save() {
        // only changes the target sync for new contacts
        if (!isNewContact) {
            return;
        }
        var activeSource = getSelectedSource()
        if (!activeSource) {
            return;
        }

        if (!root.detail) {
            root.detail = root.contact.syncTarget
        }
        root.detail.syncTarget = activeSource
    }

    function getSelectedSource() {
        if (sources.model.count <= 0)
            return -1

        var selectedSourceId = sources.model.get(sources.selectedIndex).sourceId
        if (selectedSourceId) {
            return selectedSourceId
        } else {
            return -1
        }
    }

    function contactIsReadOnly(contact) {
        var sources = sourceModel.contacts
        var contactSyncTarget = contact.syncTarget.value(SyncTarget.SyncTarget + 1)

        for (var i = 0; i < writableSources.count; i++) {
            var source = writableSources.get(i)
            if (source.sourceId === contactSyncTarget) {
                return false
            }
        }
        return true
    }

    detail: root.contact ? contact.detail(ContactDetail.SyncTarget) : null
    implicitHeight: root.isNewContact &&  sources.model && (sources.model.count > 1) ? myHeight : 0
    visible: height > 0

    ContactModel {
        id: sourceModel

        manager: ContactManager.manager()
        autoUpdate: false
        onCollectionsChanged: {
            console.log("Collections changed")
            if (collections.length > 0) {
                writableSources.reload()
                root.changed()
            }
        }
        Component.onCompleted: updateCollections()
    }

    ListModel {
        id: writableSources

        function reload() {
            console.log("reloading")
            clear()

            // filter out read-only sources
            var collections = sourceModel.collections
            if (collections.length === 0) {
                return
            }

            var data = []
            for(var i = 0; i < collections.length; i++) {
                var collection = collections[i]
                console.log("got collection " + collection.collectionId)
                if (collection.collectionId != ContactManager.aggregateCollection) {
                    data.push({'sourceId': collection.collectionId,
                               'sourceName': collection.name,
                               'accountId': 0,
                               'accountProvider': '',
                               'readOnly': false,
                               'isPrimary': false,
                                })
                }
            }

            data.sort(function(a, b) {
                var valA = a.accountId
                var valB = b.accountId
                if (a.accountId == b.accountId) {
                    valA = a.sourceName
                    valB = b.sourceName
                }

                if (valA == valB) {
                    return 0
                } else if (valA < valB) {
                    return -1
                } else {
                    return 1
                }
            })

            var primaryIndex = 0
            for (var i in data) {
                if (data[i].isPrimary) {
                    primaryIndex = i
                }
                append(data[i])
                console.log("appended item")
            }

            // select primary account
            sources.selectedIndex = primaryIndex
        }
    }

    Label {
        id: label

        text: i18n.dtr("address-book-app", "Addressbook")
        anchors {
            left: parent.left
            top: parent.top
            right: parent.right
            margins: units.gu(2)
        }
        height: units.gu(4)
    }

    ThinDivider {
        id: divider

        anchors {
            top: label.bottom
            leftMargin: units.gu(2)
            rightMargin: units.gu(2)
        }
   }

    OptionSelector {
        id: sources

        model: writableSources
        anchors {
            left: parent.left
            leftMargin: units.gu(2)
            top: divider.bottom
            topMargin: units.gu(2)
            right: parent.right
            rightMargin: units.gu(2)
            bottom: parent.bottom
            bottomMargin: units.gu(2)
        }

        delegate: OptionSelectorDelegate {
            text: {
                if ((sourceId != "system-address-book") && (accountProvider == "")) {
                    return i18n.dtr("address-book-app", "Personal - %1").arg(sourceName)
                } else {
                    return sourceName
                }
            }
            height: units.gu(4)
        }

        containerHeight: sources.model && sources.model.count > 4 ? itemHeight * 4 : sources.model ? itemHeight * sources.model.count : 0
    }
}
