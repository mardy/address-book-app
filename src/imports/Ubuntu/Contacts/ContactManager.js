var defaultManager = "org.nemomobile.contacts.sqlite"
var aggregateCollection = "qtcontacts:org.nemomobile.contacts.sqlite::636f6c2d31"

function manager()
{
    return (typeof(QTCONTACTS_MANAGER_OVERRIDE) !== "undefined") &&
           (QTCONTACTS_MANAGER_OVERRIDE != "") ?
           QTCONTACTS_MANAGER_OVERRIDE : defaultManager
}
