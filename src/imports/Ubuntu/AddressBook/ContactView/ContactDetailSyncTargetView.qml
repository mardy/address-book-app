/*
 * Copyright (C) 2014 Canonical, Ltd.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import Ubuntu.Components 1.3
import QtContacts 5.0
import Ubuntu.Contacts 0.1

ContactDetailGroupWithTypeView {
    id: root

    // does not show the field if there is only one addressbook
    function filterDetails(details) {
        var result = []

        var collections = sourceModel.collections
        if (collections.length <= 1) {
            return result;
        }

        // The actual return value does not matter, only the amount of
        // elements does. Since a contact has always one collection, just
        // return a list with one element
        result.push(1)
        return result
    }

    title: i18n.dtr("address-book-app", "Addressbook")
    defaultIcon: "image://theme/contact-group"
    detailType: ContactDetail.SyncTarget
    typeModel: null
    activeFocusOnTab: false

    fields: [ SyncTarget.SyncTarget ]

    ContactModel {
        id: sourceModel

        manager: ContactManager.manager()
        autoUpdate: false
        Component.onCompleted: updateCollections()
    }

    detailDelegate: BasicFieldView {
        values: [collectionLabel()]

        height: implicitHeight
        width: root.width
        activeFocusOnTab: false

        function collectionLabel()
        {
            var collections = sourceModel.collections
            for (var i = 0; i < collections.length; i++) {
                var collection = collections[i]
                if (collection.collectionId != root.contact.collectionId) {
                    continue
                }

                return i18n.dtr("address-book-app", "Personal - %1").arg(collection.name)
            }
            // This should never happen
            return ""
        }
    }
}
